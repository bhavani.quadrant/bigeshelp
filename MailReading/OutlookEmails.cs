﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailReading
{
    public class OutlookEmails
    {
        public string InitiatedBy { get; set; }
        public string Role { get; set; }
        public string Date { get; set; }
        public string IssueReported { get; set; }
        public string AcknowledgedBy { get; set; }


        public static List<OutlookEmails> ReadOutlookFolderContent(string FolderName)
        {
            Application application = null;
            NameSpace nameSpacedetails = null;
            Items items = null;



            application = new Application();
            nameSpacedetails = application.GetNamespace("MAPI");
            var Session = nameSpacedetails.Session;



            List<OutlookEmails> listoutlookEmails = null;
            Folder inBox = (Folder)nameSpacedetails.Session.GetDefaultFolder
                    (OlDefaultFolders.olFolderInbox);
            try
            {
                //application.ActiveExplorer().CurrentFolder = inBox.
                //    Folders[FolderName];

                DateTime start = DateTime.Now;
                DateTime end = start.AddDays(-2);
                items = application.ActiveExplorer().CurrentFolder.Items;

                string sFilter = "[ReceivedTime] <='" + string.Format("{0:D}", start) + "'" + " AND [ReceivedTime] >='" + string.Format("{0:D}", end) + "'";
                //Format -->[ReceivedTime] <='Thursday, April 23, 2020' AND [ReceivedTime] > 'Wednesday, April 22, 2020'


                items = items.Restrict(sFilter);
                items.Sort("[Subject]", true);

                int count = items.Count;
                if (count > 0)
                {
                    listoutlookEmails = new List<OutlookEmails>();
                    foreach (MailItem mailItem in items)
                    {
                        var SenderEmailId = string.Empty;
                        OutlookEmails outlookEmails = new OutlookEmails();
                        outlookEmails.InitiatedBy = mailItem.SenderName;
                        outlookEmails.AcknowledgedBy = mailItem.To;
                        outlookEmails.IssueReported = mailItem.Subject;
                        outlookEmails.Date = string.Format("{0:G}", mailItem.SentOn);
                        //var mailbody = mailItem.HTMLBody;
                        //var matches = System.Text.RegularExpressions.Regex.Matches(mailbody, @"<a\shref=""(?<url>.*?)"">(?<text>.*?)</a>");
                        //foreach (System.Text.RegularExpressions.Match m in matches)
                        //{
                        //    var URL = m.Groups["url"].Value + " -- Text = " + m.Groups["text"].Value;
                        //}

                        AddressEntry sender = mailItem.Sender;
                        if (sender.AddressEntryUserType == OlAddressEntryUserType.olExchangeUserAddressEntry
                              || sender.AddressEntryUserType == OlAddressEntryUserType.olExchangeRemoteUserAddressEntry)
                        {
                            //Use the ExchangeUser object PrimarySMTPAddress
                            ExchangeUser exchUser =
                                sender.GetExchangeUser();
                            if (exchUser != null)
                            {
                                SenderEmailId = exchUser.PrimarySmtpAddress;
                            }
                            else
                            {
                                SenderEmailId = "";
                            }
                        }
                        if (!string.IsNullOrEmpty(SenderEmailId))
                        {
                            if (SenderEmailId.Substring(0, 2) == "v-")
                            {
                                outlookEmails.Role = "BIG ES ENGINEER";
                            }
                            else
                            {
                                outlookEmails.Role = "DEVELOPER";
                            }
                        }
                        listoutlookEmails.Add(outlookEmails);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("There is no folder named " + FolderName +
                    ".", "Find Folder Name" + ex.Message);
            }
            finally
            {
                releaseCOmObj(items);
                releaseCOmObj(nameSpacedetails);
                releaseCOmObj(application);
            }
            return listoutlookEmails;
        }
        public static void releaseCOmObj(Object obj)
        {
            if (obj != null)
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
        }
    }
}
