﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailReading
{
    class Program
    {
        static void Main(string[] args)
        {
            var mails = OutlookEmails.ReadOutlookFolderContent("Big Es Help");
            DateTime dateTime = DateTime.Now;
            if (mails != null)
            {
                string fileName = "BigEsHelp_" + dateTime.ToString("dd_MM_yyyy_hh_mm_ss") + ".xlsx";
                string customExcelSavingPath = "C:\\RekhaProjects\\MailReading\\ExcelFiles" + "\\" + fileName;
                ExportExcel.GenerateExcel(ListToDataTable.ConvertToDataTable(mails), customExcelSavingPath);
                Console.WriteLine("Report generated successfully");
            }
            else
            {
                Console.WriteLine("No data available");
            }
            Console.ReadKey();
        }
    }
}
